# Code Copier

## Why would you do this?

[Because I'm an engineer.](https://xkcd.com/1319)

![https://imgs.xkcd.com/comics/automation.png](https://imgs.xkcd.com/comics/automation.png)

## Why would I want this?
This is a tiny utility that adds a copy button to `<pre>` blocks on pages that meet these patterns: `"*://*.mediawiki.org/wiki/*", "*://localhost/wiki/*", "*://*.wikimedia.org/*"`.

## Installation
1. Clone or download this to a handy location on your computer.
2. Manually install the extension according to your browser's idiosyncracies. 
    - Firefox Desktop
        1. Navigate to [about:debugging](about:debugging). 
        2. Select "This Firefox" on the lefthand side
        3. Click "Load Temporary Add-on..."
        4. Navigate to the extension directory and select any file. 
    - Chrome Desktop
        1. Navigate to [chrome://extensions](chrome://extensions/).
        2. Enable developer mode in the upper right if it's not already enabled.
        3. Click "Load Unpacked" button at top left.
        4. Select the extension directory. (Note: Chrome throws a warning because of the manifest version I'm currently using; it still works. I'll fix that later.)


## License
This is licensed under the MIT license.

## Project status
Currently, this little utility is ... heroically bare bones. All it does is add a little "copy" button to the upper right corner of code blocks. It has not been tested extensively. If you notice something weird, let me know! 

## Icon Attribution

<a href="https://commons.wikimedia.org/wiki/File:Opossum_2.jpg">Cody Pope</a>, <a href="https://creativecommons.org/licenses/by-sa/2.5">CC BY-SA 2.5</a>, via Wikimedia Commons
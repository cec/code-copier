const preElementsArray = Array.from(document.getElementsByTagName('pre'))
preElementsArray.forEach(pre => addCopyButton(pre))

function addCopyButton(el) {
    el.style.position = "relative"
    const copyDiv = createCopyButton(el)
    el.appendChild(copyDiv)
}

function createCopyButton(parentElement) {
    const copyButton = document.createElement("button");
    copyButton.classList.add("copy-button")
    copyButton.style.position = "absolute"
    copyButton.style.top = "0px"
    copyButton.style.right = "0px"
    copyButton.style.cursor = "pointer"
    copyButton.innerText = "copy"
    copyButton.addEventListener("click", function copyHandler(){
        copyTextBlock(parentElement)
    })
    return copyButton
}
function copyTextBlock(element) {
    const elementClone = element.cloneNode(true)
    const copyButtonIndex = elementClone.childNodes.length - 1
    elementClone.removeChild(elementClone.childNodes[copyButtonIndex])
    navigator.clipboard.writeText(elementClone.innerText.trim())
}